from raven.contrib.django.raven_compat.models import client
from django.template.loader import render_to_string
from django.shortcuts import HttpResponse, render
from postmark import PMMail, PMBatchMail
from django.conf import settings
from premailer import transform
import base64
import magic
import requests
import json
from django.db import connection
from django.core.paginator import Paginator, InvalidPage, EmptyPage
from .constants import ELEMENTOS_PAGINA
from . import forms
from django.utils import timezone
import datetime
import xlrd
from decimal import Decimal
from django.conf import settings
from crontab import CronTab
import logging
from django.db.models import Q, F, Sum


def to_datetime(date, max=False, use_localtime=True, min=False):
    """
    Convierte un datetime naive en aware.
    """
    if max and min:
        raise ValueError(
            u"Los argumentos max y min deben ser mutuamente excluyentes"
        )

    if hasattr(date, "tzinfo") and date.tzinfo and not min and not max:
        return date

    if not isinstance(date, (datetime.date, datetime.datetime)):
        return date

    dt = datetime.datetime
    current_tz = timezone.get_current_timezone() if use_localtime else pytz.utc

    t = dt.min.time()

    # si date es datetime conservamos la hora que trae
    if not min and isinstance(date, (datetime.datetime,)):
        t = date.time()

    if max:
        t = dt.max.time()

    if settings.USE_TZ:
        return current_tz.localize(dt.combine(date, t))

    return timezone.localtime(dt.combine(date, t))


def date_to_str(date, to_date=False, short_year=False):

    if not date:
        return ""

    if short_year:
        date_format = "%d/%m/%y"
    else:
        date_format = "%d/%m/%Y"

    datetime_format = "%s %s" % (date_format, "%H:%M:%S")

    if isinstance(date, (datetime.datetime)):
        local_date = timezone.localtime(date)
        if to_date:
            return local_date.strftime(date_format)

        return local_date.strftime(datetime_format)

    return date.strftime(date_format)


def send_email(
    *,
    to,
    subject,
    text=None,
    files=[],
    template=None,
    reply_to=None,
    template_str=None
):
    """
    funcion para envío de correos.
    """
    body_message = text

    if template:
        if not isinstance(template, dict):
            raise ValueError(
                "template debe ser un diccionario ejemplo "
                '{"name": "test.html", "context": {}}'
            )

        context_func_path = getattr(settings, "MISC_TOOLS_EMAIL_CONTEXT", None)
        context = template.get("context", {})

        if context_func_path:
            from importlib import import_module
            func_name = context_func_path.split(".")[-1]
            module_path = ".".join(context_func_path.split(".")[:-1])
            m = import_module(module_path)
            context_func = getattr(m, func_name)
            context.update(context_func(context.get("request", {})))

        body_message = render_to_string(template.get("name"), context=context)

    if template_str:
        body_message = template_str

    if not body_message:
        raise ValueError("El cuerpo del mensaje no fué especificado.")

    messages = []
    attachments = []

    for fb in files:
        try:
            data = fb["data"].read()
        except:
            data = fb["data"]

        attachments.append(
            (fb["name"], base64.b64encode(data).decode(), fb["content_type"])
        )

    for email in to:
        message = PMMail(
            subject=subject,
            text_body=text,
            html_body=transform(body_message),
            attachments=attachments,
            to=email,
            reply_to=reply_to,
        )
        messages.append(message)

    try:
        mailer = PMBatchMail(messages=messages)
        return mailer.send()
    except Exception as e:
        send_to_sentry(e)
        return False


def render_pdf(template, name, context={}, opciones={}, debug=None):
    # si nos pasan el request en el context, llamamos a pdf_context
    # para tener el context base en el template del pdf
    if not "settings" in context:
        context["settings"] = settings

    # if "request" in context:
    from homedepot.context_processors import pdf_context

    _context = pdf_context(context.get("request"))

    for k, v in _context.items():
        if not k in context:
            context[k] = v

    url = settings.PDFGEN_URL
    html_str = render_to_string(template, context)

    if debug == "html":
        return HttpResponse(html_str)

    try:
        api_headers = {
            "Tenant": "homedepot",
            "User-Agent": "HomeDepot",
            "Custom-Template": template,
        }
        api_json = {"filename": name, "html": html_str, "options": opciones}
        api_call = requests.post(url, json=api_json, headers=api_headers)
    except requests.exceptions.ConnectionError as e:
        raise e

    if not api_call.ok:
        error = "PDF Error {}: {}".format(api_call.url, api_call.content)
        raise Exception(error)

    response = HttpResponse(api_call.content)
    response["Content-Type"] = api_call.headers.get("Content-Type")
    response["Content-Disposition"] = api_call.headers.get(
        "Content-Disposition"
    )
    return response


def send_to_sentry(e, data={}):
    """
    Docs.
    """
    try:
        return client.captureMessage(e, extra=data)
    except Exception as e:
        pass


def error_response(request, errmsg):
    """
    Docs.
    """
    return render(request, "misc_tools/error_response.html", {"errmsg": errmsg})


def success_response(request, msg):
    """
    Docs.
    """
    return render(request, "misc_tools/success_response.html", {"msg": msg})


def json_response(something, allow_origin=False, dump_json=True, status=200):
    """
    Docs.
    """
    data = something
    if dump_json:
        data = json.dumps(something)

    response = HttpResponse(
        data, content_type="application/json; charset=utf8", status=status
    )
    response["Cache-Control"] = "no-cache"
    if allow_origin:
        response["Access-Control-Allow-Origin"] = "*"
        response["Access-Control-Allow-Methods"] = "POST, GET, OPTIONS"
        response["Access-Control-Max-Age"] = "1000"
        response["Access-Control-Allow-Headers"] = "*"

    return response


def list_view(
    request,
    objects,
    template,
    *,
    context={},
    nodo_actual="",
    title="",
    with_count=True
):
    """
    Recibe un request, un queryset, y un template, opcionalmente puede recibir
    un diccionario para renderear el template.

    Regresa un resonse ya rendereado con la página específica, funciona
    para generar los listados del sistema en general.

    Por defecto asigna un tamaño del primer valor que esté en la constante
    ELEMENTOS_PAGINA
    """
    try:
        iter(objects)
    except TypeError as te:
        RuntimeError(
            "El atributo objects debe ser iterable, no {}".format(objects)
        )

    default_page_size = ELEMENTOS_PAGINA[0][0]
    try:
        page_size = int(request.GET.get("page_size", default_page_size))
    except ValueError:
        page_size = default_page_size

    try:
        page = int(request.GET.get("page", 1))
    except ValueError:
        page = 1

    # print(context.get("objects"))
    # print(objects)
    # if not context.get("objects") is None:
    #    raise RuntimeError(
    #        "El diccionario de contexto no debe incluir la "
    #        "variable objects, ya que será redefinida con la página"
    #    )

    paginator = Paginator(objects, page_size)

    try:
        listado = paginator.page(page)
    except (EmptyPage, InvalidPage):
        listado = paginator.page(paginator.num_pages)

    if page < 5:
        _page = page
        page_ = 10 - page
    else:
        _page = 5
        page_ = 5

        if (page + 5) > listado.paginator.num_pages:
            page_ = listado.paginator.num_pages - page
            _page = 10 - page_

    pages_ant = page - _page if (page - _page) > 0 else 0
    page_range = list(listado.paginator.page_range)[pages_ant : page + page_]

    context["page_range"] = page_range
    context["list"] = listado
    context["objects"] = listado.object_list
    context["title"] = title

    if context.get("filtro_form") is None:
        context["filtro_form"] = forms.FiltroForm(request.GET)

    if with_count:
        context["objects_count"] = listado.paginator.count

    return render(request, template, context)


def validate_file(
    archivo,
    *,
    imagen=False,
    pdf=False,
    zipfiles=False,
    excel=False,
    word=False,
    csv=False,
    xml=False,
    return_mimes_validos=False
):
    """
    Docs.
    """
    mimes_validos = []

    if imagen:
        mimes_validos = mimes_validos + [
            "image/png",
            "image/jpeg",
            "image/x-ms-bmp",
        ]

    if pdf:
        mimes_validos = mimes_validos + ["application/pdf"]

    if zipfiles:
        mimes_validos = mimes_validos + ["application/zip"]

    if word:
        mimes_validos = mimes_validos + [
            # .doc
            "application/msword",
            # .docx
            "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
        ]

    if excel:
        mimes_validos = mimes_validos + [
            # .xls
            "application/vnd.ms-excel",
            # .xlsx
            "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
            # en local no es necesario pero en el servidor
            # algunos archivos excel dice que son del mime: application/zip
            "application/zip",
        ]

    if csv:
        mimes_validos = mimes_validos + [
            "text/comma-separated-values",
            "text/csv",
            "application/csv",
            "application/excel",
            "application/vnd.ms-excel",
            "application/vnd.msexcel",
            "text/anytext",
        ]

    if xml:
        mimes_validos = mimes_validos + [
            "text/xml",
            "application/xml",
        ]

    if return_mimes_validos:
        return (
            magic.from_buffer(archivo.read(), mime=True) in mimes_validos,
            ", ".join(mimes_validos),
        )
    return magic.from_buffer(archivo.read(), mime=True) in mimes_validos


def get_tenant():
    """
    Regresa el cliente de la BD central de clientes
    """
    try:
        return connection.tenant
    except AttributeError:
        return None


def get_user_ip(request):
    """
    REGRESA LA IP PUBLICA DEL USUARIO
    """
    x_forwarded_for = request.META.get("HTTP_X_FORWARDED_FOR")
    if x_forwarded_for:
        ip = x_forwarded_for.split(",")[0]
    else:
        ip = request.META.get("REMOTE_ADDR")
    return ip


def localtime(time=None):
    """
    Retorna un datetime con el time zone activo.
    """
    if not time:
        time = timezone.now()

    elif not isinstance(time, (datetime.datetime)):
        return time

    if not settings.USE_TZ:
        return time

    return timezone.localtime(time)


def get_hoja_excel(archivo, nc, extensiones=[]):
    """
    Obtiene la hojda de excel y valida el numero de columnas y/o extension.
    """
    wb = xlrd.open_workbook(file_contents=archivo.read())
    hoja = wb.sheet_by_index(0)
    nc_invalido = False

    if nc:
        if isinstance(nc, int) and hoja.ncols != nc:
            nc_invalido = True
        if isinstance(nc, list) and not hoja.ncols in nc:
            nc_invalido = True

    if nc_invalido:
        if wb.nsheets > 1:
            return (
                hoja,
                "Por seguridad y evitar confusiones con la información "
                "procesada, el archivo de excel debe contener solo una hoja.",
            )

        return (
            hoja,
            "El número de columnas debe ser {}, en el archivo se "
            "detectaron {} columnas.".format(nc, hoja.ncols),
        )

    if extensiones:
        if not archivo.name.split(".")[-1].lower() in extensiones:
            return hoja, "La extensión no es correcta"

    return hoja, False


def xls_text_value(cell):
    """
    Docs.
    """
    if isinstance(cell, str):
        return str(cell.strip())

    cell_value = cell.value

    if cell.ctype in (2, 3) and cell_value == string_to_int(cell_value):
        return str(string_to_int(cell_value))

    if isinstance(cell_value, str):
        return str(cell_value.strip())

    return str(cell_value)


def to_int(s):
    """
    Docs.
    """
    try:
        return int(s)
    except:
        return 0


def xls_int_value(val):
    """
    Docs.
    """
    return to_int(val)


def xls_decimal_value(val):
    """
    Docs.
    """
    return to_decimal(val)


def to_decimal(s):
    """
    Docs.
    """
    import math

    try:
        s = str(s)
        s = s.replace("$", "")
        d = Decimal("".join(s.split(",")))
        return d if not math.isnan(d) else 0
    except:
        return Decimal("0")


def string_to_int(s):
    """
    Docs
    """
    return to_int(s)


def to_precision_decimales(valor_decimal, precision=2):
    from decimal import Decimal, ROUND_HALF_UP

    if not valor_decimal:
        return Decimal("0.00")
    return Decimal("%s" % valor_decimal).quantize(
        Decimal("0.%0*d" % (precision, 1)), ROUND_HALF_UP
    )


def get_objetos_relacionados(excepcion):
    """
    Docs
    """
    relacionados = []
    for o in excepcion.protected_objects:
        relacionados.append(
            "{} con el id {}".format(o._meta.verbose_name, o.id)
        )

    return "\n".join(relacionados)


def set_crontab(
    func,
    *,
    minute=None,
    setall=None,
    delete=False,
    cron_comment=None,
    command_opts={},
    auto_delete=False
):
    """
    Docs
    """
    auto_comment = cron_comment is None
    if not cron_comment:
        cron_comment = func

    pycmd = settings.PYTHON_PATH
    base_dir = getattr(settings, "BASE_DIR")
    command = "{} {}/manage.py crontab {} ".format(pycmd, base_dir, func)

    if command_opts:
        # actualiza el comentario concatenando el id del objeto para que
        # sea unico.
        if "--id" in command_opts and auto_comment:
            cron_comment += "_{}".format(command_opts["--id"])

        opts = []
        for opt, val in command_opts.items():
            opts.append(str(opt))
            opts.append(str(val))

        command += " ".join(opts)

    if auto_delete:
        command += " --delete-crontab {}".format(cron_comment)

    crontab_user = settings.CRONTAB_USER
    cron = CronTab(user=crontab_user)
    job = None

    for c in cron.find_comment(cron_comment):
        job = c
        break

    if delete:
        if job:
            job.delete()
    else:
        if job is None:
            job = cron.new(comment=cron_comment, command=command)

        else:
            job.command = command

        if setall:
            job.setall(setall)

    cron.write()


def delete_crontab(cron_comment):
    """
    Docs
    """
    if not cron_comment:
        return None

    crontab_user = settings.CRONTAB_USER
    cron = CronTab(user=crontab_user)
    job = None

    for c in cron.find_comment(cron_comment):
        job = c
        break

    if job:
        job.delete()

    cron.write()


def aggregate_sum(qs, field):

    if type(field) == str:
        field = F(field)

    if qs is None:
        return to_decimal("0")
    else:
        monto = qs.aggregate(suma=Sum(field))

    return to_decimal(monto["suma"])


def safe_division(n1, n2):
    """
    Recibe un numerador y denominador y regresa la división 
    resultante en caso de que uno de los dos valores sea 0 regresa 0.
    """
    if not n1 or not n2:
        return 0

    return n1 / n2


def add_months(fecha, n=1, day=None):
    from datetime import datetime

    month = fecha.month
    year = fecha.year
    from .functions import ultimo_dia_mes

    if not day:
        day = fecha.day

    for i in range(n):

        month += 1
        if month == 13:
            month = 1
            year += 1

    try:
        fecha = datetime(year, month, day)
    except ValueError:
        fecha = datetime.combine(
            ultimo_dia_mes(year, month), datetime.min.time()
        )
    return to_datetime(fecha)
