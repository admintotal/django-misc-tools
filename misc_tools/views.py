from django.shortcuts import render
from .functions import json_response
from django.conf import settings
import requests

def get_colonias(request):
    """
    Regresa un  listado de colonias a partir de un código postal.
    """
    cp = request.GET.get("cp")

    if not cp:
        return json_response({
            "status": "error",
            "message": "El parámetro GET cp no fué especificado"
        })

    try:
        url = settings.UBICACIONES_URL.format(cp=cp)
        return json_response(requests.get(url).json())
    except Exception as e:
        #send_to_sentry(e)
        return json_response({
            "status": "error",
            "message": "Hubo un error al consultar la información."
        })
