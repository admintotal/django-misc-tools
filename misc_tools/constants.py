MESES = (
    (1,'Enero'),
    (2,'Febrero'),
    (3,'Marzo'),
    (4,'Abril'),
    (5,'Mayo'),
    (6,'Junio'),
    (7,'Julio'),
    (8,'Agosto'),
    (9,'Septiembre'),
    (10,'Octubre'),
    (11,'Noviembre'),
    (12,'Diciembre'),
)
ELEMENTOS_PAGINA = (
    (25, "25"),
    (50, "50"),
    (100, "100"),
    (1000, "1000"),
)

SOLO_FECHA_ANTERIOR = 0
SOLO_FECHA_POSTERIOR = 1
DESDE_1900 = 2