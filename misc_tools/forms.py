import datetime
from django import forms
from django.contrib.auth.models import User
from django.forms.widgets import ClearableFileInput
from . import functions
from .constants import (
    ELEMENTOS_PAGINA,
    MESES,
    SOLO_FECHA_ANTERIOR,
    SOLO_FECHA_POSTERIOR,
    DESDE_1900,
)
from .models import Direccion

EMPTY_CHOICE = (("", "---"),)


class CustomModelForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        usuario = kwargs.pop("usuario", None)
        super(CustomModelForm, self).__init__(*args, **kwargs)


class CustomForm(forms.Form):
    def __init__(self, *args, **kwargs):
        usuario = kwargs.pop("usuario", None)
        super(CustomForm, self).__init__(*args, **kwargs)


class BootstrapForm(CustomForm):
    def __init__(self, *args, **kwargs):
        super(BootstrapForm, self).__init__(*args, **kwargs)
        for f in self.visible_fields():
            fields_exclude = [
                forms.FileField,
                forms.BooleanField,
                forms.ModelMultipleChoiceField,
            ]
            exclude = [
                True
                for x in fields_exclude
                if isinstance(self.fields[f.name], x)
            ]
            if not exclude:
                css_class = self.fields[f.name].widget.attrs.get("class", "")

                if f.name == "banco":
                    css_class += " autocomplete-banco"

                self.fields[f.name].widget.attrs.update(
                    {"class": css_class + " form-control"}
                )
            else:
                self.fields[f.name].widget.attrs.update(
                    {"style": "max-width:100%"}
                )

    def clean(self):
        cleaned_data = super(BootstrapForm, self).clean()
        return cleaned_data


class BootstrapModelForm(CustomModelForm):
    def __init__(self, *args, **kwargs):
        super(BootstrapModelForm, self).__init__(*args, **kwargs)
        for f in self.visible_fields():
            fields_exclude = [
                forms.FileField,
                forms.BooleanField,
                forms.ModelMultipleChoiceField,
            ]
            exclude = [
                True
                for x in fields_exclude
                if isinstance(self.fields[f.name], x)
            ]
            if not exclude:
                css_class = self.fields[f.name].widget.attrs.get("class", "")

                if f.name == "banco":
                    css_class += " autocomplete-banco"

                self.fields[f.name].widget.attrs.update(
                    {"class": css_class + " form-control"}
                )
            else:
                self.fields[f.name].widget.attrs.update(
                    {"style": "max-width:100%"}
                )

    def add_method(self, method, name=None):
        if name is None:
            name = method.func_name
        setattr(self.__class__, name, method)

    def clean(self):
        cleaned_data = super(BootstrapModelForm, self).clean()
        return cleaned_data


class CalendarWidget(forms.DateInput):
    def __init__(
        self,
        fecha_anterior=True,
        fecha_posterior=True,
        datetime=False,
        attrs={},
    ):
        clases = ""
        if datetime:
            clases = "datetimepicker"
        else:
            clases = "datepicker"

        attrs["autocomplete"] = "off"

        if fecha_anterior is False:
            clases = "%s validar_fecha_anterior" % clases

        if fecha_posterior is False:
            clases = "%s validar_fecha_posterior" % clases

        attrs["class"] = clases
        super(CalendarWidget, self).__init__(attrs=attrs)


class CalendarDateTimeWidget(forms.DateTimeInput):
    def __init__(
        self,
        fecha_anterior=True,
        fecha_posterior=True,
        datetime=False,
        attrs={},
    ):
        clases = ""
        if datetime:
            clases = "datetimepicker"
        else:
            clases = "datepicker"

        if fecha_anterior is False:
            clases = "%s validar_fecha_anterior" % clases

        if fecha_posterior is False:
            clases = "%s validar_fecha_posterior" % clases

        attrs["class"] = clases
        super(CalendarDateTimeWidget, self).__init__(attrs=attrs)


def validar_fecha(fecha):
    if fecha < datetime.datetime(1900, 1, 1).date():
        raise forms.ValidationError(u"El año no puede ser menor a 1900.")


def validar_fecha_anterior(fecha):
    if fecha < datetime.date.today():
        raise forms.ValidationError(
            u"No se puede seleccionar una fecha menor al día de hoy"
        )


def validar_fecha_posterior(fecha):
    if fecha > datetime.date.today():
        raise forms.ValidationError(
            u"No se puede seleccionar una fecha mayor al día de hoy"
        )


class CalendarDateField(forms.DateField):
    def __init__(
        self,
        fecha_anterior=True,
        fecha_posterior=True,
        attrs={},
        *args,
        **kwargs
    ):
        calendar_widget = CalendarWidget(
            fecha_anterior=fecha_anterior,
            fecha_posterior=fecha_posterior,
            datetime=False,
        )
        validators = [validar_fecha]
        if fecha_anterior is False:
            validators.append(validar_fecha_anterior)

        if fecha_posterior is False:
            validators.append(validar_fecha_posterior)

        super(CalendarDateField, self).__init__(
            input_formats=("%d/%m/%Y", "%Y-%m-%d"),
            widget=calendar_widget,
            validators=validators,
            *args,
            **kwargs
        )


class CalendarDateTimeField(forms.DateTimeField):
    def __init__(
        self,
        fecha_anterior=True,
        fecha_posterior=True,
        attrs={},
        *args,
        **kwargs
    ):
        calendar_widget = CalendarWidget(
            fecha_anterior=fecha_anterior,
            fecha_posterior=fecha_posterior,
            datetime=True,
        )

        if "widget" in kwargs:
            calendar_widget = kwargs["widget"]
            del kwargs["widget"]

        super(CalendarDateTimeField, self).__init__(
            input_formats=("%d/%m/%Y %H:%M", "%d/%m/%y %H:%M:%S"),
            widget=calendar_widget,
            *args,
            **kwargs
        )


class FiltroForm(BootstrapForm):
    SI_NO_OP = (("", "------------"), (1, u"Sí"), (0, u"No"))

    page_size = forms.ChoiceField(choices=ELEMENTOS_PAGINA, required=False)
    year = forms.ChoiceField(required=False)
    month = forms.ChoiceField(choices=(("", "-----")) + MESES, required=False)

    q = forms.CharField(max_length=100, required=False)
    desde = CalendarDateField(required=False)
    hasta = CalendarDateField(required=False)
    usuario = forms.ModelChoiceField(
        queryset=User.objects.none(), required=False
    )
    fecha = CalendarDateField(required=False)
    desde_month = forms.ChoiceField(choices=MESES, required=False)
    desde_year = forms.ChoiceField(required=False)
    hasta_month = forms.ChoiceField(choices=MESES, required=False)
    hasta_year = forms.ChoiceField(required=False)
    si_no = forms.ChoiceField(choices=SI_NO_OP, required=False)
    booleano = forms.BooleanField(required=False)

    choices = forms.ChoiceField(choices=[], required=False)
    choices2 = forms.ChoiceField(choices=[], required=False)

    def clean_q(self):
        """
        Se limpian los espacios extra.
        """
        return " ".join(self.cleaned_data["q"].split())

    def clean(self):
        cleaned_data = self.cleaned_data
        desde = functions.to_datetime(cleaned_data.get("desde"))
        hasta = functions.to_datetime(cleaned_data.get("hasta"), max=True)
        year = cleaned_data.get("year")
        month = cleaned_data.get("month")

        return cleaned_data

    def __init__(self, *args, **kwargs):
        super(FiltroForm, self).__init__(*args, **kwargs)


class CustomImageWidget(ClearableFileInput):
    template_name = "utils/custom_file_input.html"


class DireccionForm(BootstrapModelForm):
    class Meta:
        model = Direccion
        fields = (
            'municipio',
            'calle',
            'colonia',
            'cp',
            'localidad',
            'telefono',
            'email',
            'numero_e',
            'numero_i',
            'pais_residencia',
            'estado',
            'latitud',
            'longitud',
            'referencia',
        )  
    
    calle = forms.CharField(widget=forms.TextInput(), required = False)
    colonia = forms.CharField(widget=forms.TextInput(), required = False)
    cp = forms.CharField(widget=forms.TextInput(), required = False)
    telefono = forms.CharField(widget=forms.TextInput(), required = False, label=u"Teléfono")
    email = forms.EmailField(required=False)
    numero_e = forms.CharField(widget=forms.TextInput(), required = False)
    numero_i = forms.CharField(widget=forms.TextInput(), required = False)
    pais_residencia = forms.CharField(widget=forms.TextInput(attrs={'size':'2'}), required = False)